const app = new Vue({
    el: '#app',
    data() {
        return {
            info : null,
            hombres:{
                menor20 : [],
                entre20_40 : [],
                mayor40 : []
            },
            mujeres:{
                menor20 : [],
                entre20_40 : [],
                mayor40 : []
            },
            ciudades: [],
        }
    },
    mounted() {
        axios
            .get('https://www.datos.gov.co/resource/gt2j-8ykr.json')
            .then(response => {
                this.info = response.data;
                response.data.map((item) => {
                    if (item.sexo === "F"){
                        if (item.edad >= 0 && item.edad <= 20){
                            this.mujeres.menor20.push(item);
                        } else if (item.edad > 20 && item.edad <= 40){
                            this.mujeres.entre20_40.push(item);
                        } else {
                            this.mujeres.mayor40.push(item);
                        }
                    } else  if (item.sexo === "M") {
                            if (item.edad >= 0 && item.edad <= 20) {
                                this.hombres.menor20.push(item);
                            } else if (item.edad > 20 && item.edad <= 40) {
                                this.hombres.entre20_40.push(item);
                            } else {
                                this.hombres.mayor40.push(item);
                            }
                        }
                    let index = this.ciudades.findIndex(element => element.nombre === item.ciudad_de_ubicaci_n);
                    if ( index > -1){
                        this.ciudades[index].cantidad+=1;
                    }
                    else{
                        this.ciudades.push({ nombre: item.ciudad_de_ubicaci_n, cantidad: 1})
                    }
                    this.ciudades.sort(function(a,b){
                        return b.cantidad - a.cantidad;
                    })
                })
            } )
    }
})